use chrono::prelude::*;
pub mod error;
use crate::data::error::DataError;
use std::str::FromStr;
use tokio::sync::mpsc;

pub type Timestamp = DateTime<Utc>;
pub type PriceType = f32;

#[derive(Clone, Debug)]
pub struct MarketEvent {
    pub timestamp: Timestamp,
    pub exchange: Exchange,
    pub symbol: String,
    pub data: MarketData,
}

pub trait MarketEventGenerator {
    // None may result from an err
    fn get_market_event_rx(&mut self) -> Option<mpsc::Receiver<MarketEvent>>;
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub enum Exchange {
    SSE,  // Shanghai Stock Exchange
    SZSE, // ShenZhen Stock Exchange
}

impl FromStr for Exchange {
    type Err = DataError;
    fn from_str(input: &str) -> Result<Exchange, Self::Err> {
        match input {
            "SH" | "SSE" => Ok(Exchange::SSE),
            "SZSE" | "SZ" => Ok(Exchange::SZSE),
            _ => Err(DataError::EnumError),
        }
    }
}

#[derive(Clone, Debug)]
pub enum MarketData {
    Transaction(Transaction),
    Tick(Tick),
}

#[derive(Clone, Debug)]
pub struct Transaction {
    pub price: PriceType,
    pub volume: u32,
    pub turnover: f64,
    pub bsflag: BSFlag,
    pub function_code: bool, // true = no revokes
}

impl Transaction {
    pub fn new(
        mut price: f32,
        volume: u32,
        turnover: f64,
        bsflag: &str,
        function_code: i32,
    ) -> Result<Transaction, DataError> {
        let bsflag = match bsflag {
            "B" => BSFlag::B,
            "S" => BSFlag::S,
            _ => return Err(DataError::EnumError),
        };
        let function_code = match function_code {
            1 => true,
            _ => false,
        };

        price /= 10000f32;
        Ok(Transaction {
            price,
            volume,
            turnover,
            bsflag,
            function_code,
        })
    }
}

#[derive(Clone, Debug)]
pub enum BSFlag {
    B, // Buy/主买单
    S, // Sell/主卖单
}

#[derive(Clone, Debug, Copy)]
pub struct Tick {
    // other fields seem to be irrelevant in this case
    pub n_price: PriceType,
    pub ask_price: [PriceType; 10],
    pub bid_price: [PriceType; 10],
}

impl Tick {
    pub fn new(
        n_price: &str,
        ask_price: [String; 10],
        bid_price: [String; 10],
    ) -> Result<Tick, DataError> {
        match n_price.parse::<PriceType>() {
            Ok(mut n_price) => {
                n_price /= 10000f32;
                let ask_price = parse_array(ask_price)?;
                let bid_price = parse_array(bid_price)?;
                Ok(Tick {
                    n_price,
                    ask_price,
                    bid_price,
                })
            }
            Err(e) => Err(DataError::ParseError(format!(
                "{} is not a float: {}",
                n_price, e
            ))),
        }
    }
}

fn parse_array(arr: [String; 10]) -> Result<[PriceType; 10], DataError> {
    let maybe_price: Result<Vec<PriceType>, DataError> = arr
        .map(|x| {
            if let Ok(num) = x.parse::<PriceType>() {
                Ok(num / 10000f32)
            } else {
                Err(DataError::ParseError(format!("{} is not a float", x)))
            }
        })
        .into_iter()
        .collect();
    match maybe_price {
        Ok(vec_price) => match vec_price.try_into() {
            Ok(v) => Ok(v),
            Err(e) => Err(DataError::ParseError(format!(
                "fail to convert {:?} into price array",
                e
            ))),
        },
        Err(e) => Err(e),
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn parse_array_1() {
        let arr = (0..10)
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .try_into()
            .unwrap();
        let result = parse_array(arr).unwrap();
        let er: [f32; 10] = (0..10)
            .map(|x| x as f32)
            .collect::<Vec<f32>>()
            .try_into()
            .unwrap();
        assert_eq!(result, er);
    }

    #[test]
    fn parse_array_2() {
        let arr: [String; 10] = (0..10)
            .map(|_| "abc".to_owned())
            .collect::<Vec<String>>()
            .try_into()
            .unwrap();
        let result = parse_array(arr);
        assert_eq!(
            result,
            Err(DataError::ParseError("abc is not a float".to_owned()))
        );
    }

    #[test]
    fn parse_array_3() {
        let arr = ["2.0".to_owned(), "1.a".to_owned()];
        let arr2: [String; 8] = (2..10)
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .try_into()
            .unwrap();
        let arr: [String; 10] = arr
            .into_iter()
            .chain(arr2.into_iter())
            .collect::<Vec<String>>()
            .try_into()
            .unwrap();
        let result = parse_array(arr);
        assert_eq!(
            result,
            Err(DataError::ParseError("1.a is not a float".to_owned()))
        )
    }
}
