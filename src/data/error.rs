use thiserror::Error;

#[derive(Debug, Error, PartialEq)]
pub enum DataError {
    #[error("fail to parse data")]
    ParseError(String),

    #[error("fail to fit enum")]
    EnumError,
}
