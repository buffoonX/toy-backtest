// hide the details of trader
pub mod engine;

// hide the details of historical data
pub mod data;

// hide the details relating to a specific broker
// e.g. signals provided by the broker
pub mod broker;
