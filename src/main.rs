use chrono::prelude::*;
use tokio::sync::oneshot;
use tokio::time::{sleep, Duration};
use toy_engine::engine::{trader::Trader, Engine};

use crate::toy::{data::CsvData, executor::NaiveBuyer, strategy::NaiveStrategy};

// concrete implementation
pub mod toy;

#[tokio::main]
async fn main() {
    // start_backtest().await;
    println!("start at {}", Utc::now());
    start_backtest().await;
    println!("finish at {}", Utc::now());
}

async fn start_backtest() {
    let transac_path = "/Users/lin/Documents/career/data/601012.SH.Transaction.csv";
    let tick_path = "/Users/lin/Documents/career/data/601012.SH.Tick.csv";
    let cd = CsvData::new(tick_path, transac_path, 32);

    let broker = NaiveBuyer::new(0.0);
    let strategy = NaiveStrategy::new();

    let traders = vec![Trader::new(cd, broker, strategy)];
    let (tx, rx) = oneshot::channel();
    tokio::spawn(async move {
        sleep(Duration::from_secs(200)).await;
        if let Err(e) = tx.send("".to_owned()) {
            println!("sender error: {e}");
        }
    });
    let engine = Engine::new(traders, rx);
    engine.start().await;
}
