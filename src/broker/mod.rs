use crate::broker::error::BrokerError;
use crate::data::{Exchange, MarketEvent, Timestamp};
use chrono::prelude::*;
use std::cmp::Ordering;

pub mod error;

// the strategy one can made is limited by the broker
#[derive(Debug)]
pub struct SignalEvent {
    pub exchange: Exchange,
    pub symbol: String, // stock symbol
    pub command: SignalCommand,
    pub timestamp: Timestamp,
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub enum SignalCommand {
    Buy(OrderType, u32), // 股数; 虽然a股里面最小单位是手
    Sell(OrderType, u32),
    UpdateSell(Timestamp, OrderType, OrderType, u32),
    Transaction(OrderType),
}

#[derive(PartialEq, Eq, Clone, Hash, Debug)]
pub enum OrderType {
    Market,       // 市价委托;五档优先剩余撤销
    Limit(usize), // 限价委托, Limit(1) 表示卖一
}

impl SignalEvent {
    pub fn new_buy(
        exchange: Exchange,
        symbol: String,
        quantity: u32,
        order_type: OrderType,
        timestamp: Timestamp,
    ) -> SignalEvent {
        SignalEvent {
            exchange,
            symbol,
            command: SignalCommand::Buy(order_type, quantity),
            timestamp,
        }
    }

    pub fn new_sell(
        exchange: Exchange,
        symbol: String,
        quantity: u32,
        order_type: OrderType,
        timestamp: Timestamp,
    ) -> SignalEvent {
        SignalEvent {
            exchange,
            symbol,
            command: SignalCommand::Sell(order_type, quantity),
            timestamp,
        }
    }

    pub fn new_sell_udpate(
        exchange: Exchange,
        symbol: String,
        order_ts: Timestamp,
        from: OrderType,
        to: OrderType,
        volume: u32,
        timestamp: Timestamp,
    ) -> SignalEvent {
        SignalEvent {
            exchange,
            symbol,
            command: SignalCommand::UpdateSell(order_ts, from, to, volume),
            timestamp,
        }
    }
}

pub struct SellSuccess(
    pub Exchange,
    pub String,
    pub Timestamp,
    pub OrderType,
    pub u32,
);

pub trait SignalGenerator {
    // a market_event may be read by different functions
    fn generate_signal(&mut self, market_event: &MarketEvent) -> Vec<SignalEvent>;

    fn notify_order(&mut self, order: OrderEvent);

    fn update_sell(&mut self, sell_success: SellSuccess);

    fn print_summary(&self);
}

#[derive(Clone, PartialEq, Eq)]
pub struct OrderEvent {
    pub exchange: Exchange,
    pub symbol: String,
    pub signal: SignalCommand,
    pub timestamp: DateTime<Utc>,
}

impl OrderEvent {
    pub fn new(
        exchange: Exchange,
        symbol: String,
        signal: SignalCommand,
        timestamp: Timestamp,
    ) -> OrderEvent {
        OrderEvent {
            exchange,
            symbol,
            signal,
            timestamp,
        }
    }
}

impl PartialOrd for OrderEvent {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.timestamp.cmp(&other.timestamp))
    }
}

impl Ord for OrderEvent {
    fn cmp(&self, other: &Self) -> Ordering {
        self.timestamp.cmp(&other.timestamp)
    }
}

// broker agent must be a OrderGenerator
pub trait OrderGenerator {
    fn generate_order(&mut self, signal: SignalEvent) -> Result<Option<OrderEvent>, BrokerError>;
    fn get_closed_balance(&self) -> f64;
    fn update(&mut self, market_event: &MarketEvent) -> Vec<SellSuccess>;
}
