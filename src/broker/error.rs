use thiserror::Error;

#[derive(Debug, Error)]
pub enum BrokerError {
    #[error("failed to follow the signal and generate the order(s)")]
    OrderGenerateFail,
}
