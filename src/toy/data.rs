// use crate::toy::util::MergeAscending;
use anyhow::{Context, Result};
use chrono::prelude::*;
use csv::ReaderBuilder;
use std::collections::HashMap;
use std::fs::File;
use std::str::FromStr;
use std::iter::Peekable;
use tokio::sync::mpsc;
use toy_engine::data::{
    Exchange, MarketData, MarketEvent, MarketEventGenerator, Tick, Timestamp, Transaction,
};

const ASK_PRICE_FIELD: &'static str = "nAskPrice";
const BID_PRICE_FIELD: &'static str = "nBidPrice";

pub struct CsvData {
    data_tx: Option<mpsc::Sender<MarketEvent>>,
    single_data_rx: Option<mpsc::Receiver<MarketEvent>>,
    tick_path: String,
    transac_path: String,
}

impl CsvData {
    pub fn new(tick_path: &str, transac_path: &str, capacity: usize) -> CsvData {
        let (data_tx, data_rx) = mpsc::channel(capacity);
        CsvData {
            data_tx: Some(data_tx),
            tick_path: tick_path.to_owned(),
            transac_path: transac_path.to_owned(),
            single_data_rx: Some(data_rx),
        }
    }

    pub fn start_tick(&mut self) {
        let path = self.tick_path.clone();
        if let Some(data_tx) = self.data_tx.take() {
            tokio::spawn(async move { read_ticks(path, data_tx).await });
        }
    }

    pub fn start_transac(&mut self) {
        let path = self.transac_path.clone();
        if let Some(data_tx) = self.data_tx.take() {
            tokio::spawn(async move { read_transactions(path, data_tx).await });
        }
    }

    pub fn start(&mut self) -> Result<()> {
        if let Some(data_tx) = self.data_tx.take() {
            let ticks = ticks_iter(&self.tick_path)?.peekable();
            let transacs = transacs_iter(&self.transac_path)?.peekable();
            tokio::spawn(async move { read_all(ticks, transacs, data_tx).await });
        }
        Ok(())
    }
}

impl MarketEventGenerator for CsvData {
    fn get_market_event_rx(&mut self) -> Option<mpsc::Receiver<MarketEvent>> {
        if let Some(_) = self.data_tx {
            self.start().expect("fail to read csv!");
        }
        self.single_data_rx.take()
    }
}

async fn read_all(
    mut ticks: Peekable<impl Iterator<Item = Result<MarketEvent>>>,
    mut transacs: Peekable<impl Iterator<Item = Result<MarketEvent>>>,
    data_tx: mpsc::Sender<MarketEvent>,
) -> Result<()> {
    loop {
        match (ticks.peek(), transacs.peek()) {
            (Some(Ok(tick)), Some(Ok(transac))) => {
                data_tx
                    .send(if tick.timestamp < transac.timestamp {
                        ticks.next().unwrap().unwrap()
                    } else {
                        transacs.next().unwrap().unwrap()
                    })
                    .await?;
            }
            (Some(Err(e)), _) => {
                dbg!(e);
                ticks.next();
            }
            (_, Some(Err(e))) => {
                dbg!(e);
                transacs.next();
            }
            _ => {
                break;
            }
        }
    }
    while let Some(maybe_tick) = ticks.next() {
        match maybe_tick {
            Ok(me) => {
                data_tx.send(me).await?;
            }
            Err(e) => {
                dbg!(e);
            }
        }
    }
    while let Some(maybe_transac) = transacs.next() {
        match maybe_transac {
            Ok(me) => {
                data_tx.send(me).await?;
            }
            Err(e) => {
                dbg!(e);
            }
        }
    }
    Ok(())
}

async fn read_ticks(path: String, data_tx: mpsc::Sender<MarketEvent>) -> Result<()> {
    if let Ok(mut tick_gen) = ticks_iter(&path) {
        while let Some(Ok(e)) = tick_gen.next() {
            data_tx.send(e).await?;
        }
    }
    Ok(())
}

fn ticks_iter(path: &str) -> Result<impl Iterator<Item = Result<MarketEvent>>> {
    let file = File::open(path).context("fail to open tick csv")?;
    Ok(ReaderBuilder::new()
        .has_headers(true)
        .from_reader(file)
        .into_deserialize()
        .enumerate()
        .map(|(row_index, record)| {
            let record: HashMap<String, String> = record?;
            parse_tick_row(record, row_index)
        }))
}

fn parse_tick_row(record: HashMap<String, String>, row_index: usize) -> Result<MarketEvent> {
    let n_price = record.get("nPrice").unwrap();

    let ask_price: [String; 10] = (0..10)
        .map(|i| {
            record
                .get(&format!("{}{}", ASK_PRICE_FIELD, i + 1))
                .expect(&format!("{}{} must be in the csv!", ASK_PRICE_FIELD, i + 1))
                .clone()
        })
        .collect::<Vec<String>>()
        .try_into()
        .unwrap();

    let bid_price: [String; 10] = (0..10)
        .map(|i| {
            record
                .get(&format!("{}{}", BID_PRICE_FIELD, i + 1))
                .expect(&format!("{}{} must be in the csv!", BID_PRICE_FIELD, i + 1))
                .clone()
        })
        .collect::<Vec<String>>()
        .try_into()
        .unwrap();

    let tick = Tick::new(n_price, ask_price, bid_price).context(format!(
        "invalid row{}:\n{:?}\n",
        row_index,
        record.values()
    ))?;
    let info: Vec<&str> = record.get("chWindCode").expect("").split(".").collect();

    let exchange = Exchange::from_str(info[1])?;
    let timestamp = parse_timestamp(record.get("nTime").expect(""))?;
    Ok(MarketEvent {
        timestamp,
        exchange,
        symbol: info[0].to_owned(),
        data: MarketData::Tick(tick),
    })
}

async fn read_transactions(path: String, data_tx: mpsc::Sender<MarketEvent>) -> Result<()> {
    if let Ok(mut transac_gen) = transacs_iter(&path) {
        while let Some(Ok(e)) = transac_gen.next() {
            data_tx.send(e).await?;
        }
    }
    Ok(())
}

fn transacs_iter(path: &str) -> Result<impl Iterator<Item = Result<MarketEvent>>> {
    let file = File::open(path).context("fail to open transac csv")?;
    Ok(ReaderBuilder::new()
        .has_headers(true)
        .from_reader(file)
        .into_deserialize()
        .enumerate()
        .map(|(row_index, record)| {
            let record: HashMap<String, String> = record?;
            parse_transac_row(record, row_index)
        }))
}

fn parse_transac_row(record: HashMap<String, String>, row_index: usize) -> Result<MarketEvent> {
    let info: Vec<&str> = record
        .get("Tkr")
        .expect("Tkr must be in csv!")
        .split(".")
        .collect();

    let timestamp = parse_timestamp(record.get("Time").expect(""))?;
    let exchange = Exchange::from_str(info[1])?;
    let symbol = info[0].to_owned();
    let price: f32 = record
        .get("Price")
        .expect("Price must be in csv!")
        .parse()?;
    let volume: u32 = record
        .get("Volume")
        .expect("Volume must be in csv!")
        .parse()?;
    let turnover: f64 = record
        .get("Turnover")
        .expect("Turnover must be in csv!")
        .parse()?;
    let bsflag = record.get("BSFlag").expect("BSFlag must be in csv!");

    let function_code: i32 = record
        .get("FunctionCode")
        .expect("function code must be in csv!")
        .parse()?;

    let transac = Transaction::new(price, volume, turnover, bsflag, function_code).context(
        format!("invalid row at {}: {:?}", row_index, record.values()),
    )?;
    Ok(MarketEvent {
        exchange,
        symbol,
        timestamp,
        data: MarketData::Transaction(transac),
    })
}

fn parse_timestamp(digits: &str) -> Result<Timestamp> {
    let mut digits: u32 = digits.parse()?;
    let mut sec = digits % 100000;
    digits = (digits - sec) / 100000;
    sec /= 1000;
    let min = digits % 100;
    let hour = digits / 100;
    let now = chrono::offset::Utc::now();
    Ok(Utc
        .ymd(now.year(), now.month(), now.day())
        .and_hms(hour, min, sec)
        .with_timezone(&Utc))
}

#[cfg(test)]
mod tests {
    use chrono::Timelike;
    use toy_engine::data::MarketEventGenerator;

    use crate::toy::data::CsvData;

    use super::parse_timestamp;

    #[tokio::test]
    async fn try_tick() {
        let tick_path = "/Users/lin/Documents/career/data/601012.SH.Tick.csv";
        let mut cd = CsvData::new(tick_path, "", 4);
        let mut rx = cd.get_market_event_rx().unwrap();

        cd.start_tick();
        let mut count = 0;
        while let Some(_) = rx.recv().await {
            count += 1;
        }
        assert_eq!(count, 5101);
    }

    #[tokio::test]
    async fn try_transac() {
        let transac_path =
            "/Users/lin/Documents/career/data/601012.SH.Transaction.csv";
        let mut cd = CsvData::new("", transac_path, 4);
        let mut rx = cd.get_market_event_rx().unwrap();

        cd.start_transac();
        let mut count = 0;
        while let Some(_) = rx.recv().await {
            count += 1;
        }
        assert_eq!(count, 396938);
    }

    #[tokio::test]
    async fn try_all() {
        let transac_path = "/Users/lin/Documents/career/data/test_transac.csv";
        let tick_path = "/Users/lin/Documents/career/data/601012.SH.Tick.csv";
        let mut cd = CsvData::new(tick_path, transac_path, 32);
        let mut rx = cd.get_market_event_rx().unwrap();

        let mut count = 0;
        while let Some(_) = rx.recv().await {
            count += 1;
        }
        assert_eq!(count, 199 + 5101);
    }

    #[test]
    fn try_parse_ts() {
        let digits = "91003000";
        let dt = parse_timestamp(digits).unwrap();
        assert_eq!(dt.minute(), 10);
        assert_eq!(dt.hour(), 9);
        assert_eq!(dt.second(), 3);

        let digits = "141150000";
        let dt = parse_timestamp(digits).unwrap();
        assert_eq!(dt.hour(), 14);
        assert_eq!(dt.minute(), 11);
        assert_eq!(dt.second(), 50);
    }
}
