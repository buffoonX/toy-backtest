use std::cmp::Reverse;
use std::collections::{BinaryHeap, HashMap, VecDeque};

use chrono::{TimeZone, Utc};
use toy_engine::broker::{
    OrderEvent, OrderType, SellSuccess, SignalCommand, SignalEvent, SignalGenerator,
};
use toy_engine::data::{Exchange, MarketData, MarketEvent, PriceType, Timestamp};

pub struct NaiveStrategy {
    prices: HashMap<Exchange, HashMap<String, VecDeque<(Timestamp, PriceType)>>>,
    // min heap for buy order -- the top element is the agest buy order!
    buy_orders: BinaryHeap<Reverse<OrderEvent>>,
    // record the selling orders' rest quantity, order type and sell price
    sell_orders: HashMap<Exchange, HashMap<String, Vec<(Timestamp, OrderType, u32)>>>,
    last_buy_ts: Timestamp,
}

impl NaiveStrategy {
    pub fn new() -> NaiveStrategy {
        NaiveStrategy {
            buy_orders: BinaryHeap::new(),
            sell_orders: Default::default(),
            prices: Default::default(),
            last_buy_ts: Utc.ymd(1970, 1, 1).and_hms(0, 0, 0),
        }
    }
}

const TEN_HAND: u32 = 1000;
const FREEZE_SECONDS: i64 = 30;
const SELL_SECONDS: i64 = 60;
const SELL_CHANGE_SECONDS: i64 = 30;

impl SignalGenerator for NaiveStrategy {
    fn generate_signal(&mut self, market_event: &MarketEvent) -> Vec<SignalEvent> {
        let mut signals = vec![];

        // update existing sell ordertype if time meets
        for (exchange, stock2sell) in &mut self.sell_orders {
            for (symbol, sells) in stock2sell {
                sells
                    .iter_mut()
                    .filter(|sell| sell.1 == OrderType::Limit(1))
                    .for_each(|sell| {
                        if (market_event.timestamp - sell.0).num_seconds() >= SELL_CHANGE_SECONDS {
                            log_event(
                                market_event.timestamp,
                                &format!("move {:?} {} to market", &sell.1, sell.2),
                            );
                            signals.push(SignalEvent::new_sell_udpate(
                                exchange.to_owned(),
                                symbol.to_owned(),
                                sell.0,
                                sell.1.to_owned(),
                                OrderType::Market,
                                sell.2,
                                market_event.timestamp,
                            ));
                            sell.1 = OrderType::Market;
                        }
                    })
            }
        }

        // new buy
        let seconds = (market_event.timestamp - self.last_buy_ts).num_seconds();
        // println!("{}", seconds);
        if seconds >= FREEZE_SECONDS {
            if let Some(se) = self.buy_trigger(market_event) {
                signals.push(se);
            }
        }

        // new sell
        while let Some(Reverse(order)) = self.buy_orders.pop() {
            if (market_event.timestamp - order.timestamp).num_seconds() < SELL_SECONDS {
                self.buy_orders.push(Reverse(order));
                break;
            }

            signals.push(SignalEvent::new_sell(
                order.exchange,
                order.symbol,
                TEN_HAND,
                // 1 = 卖一
                OrderType::Limit(1),
                market_event.timestamp,
            ));
        }
        // update past prices
        self.add_price(
            market_event.exchange.clone(),
            market_event.symbol.clone(),
            market_event.timestamp,
            get_current_price(&market_event),
        );

        signals
    }

    fn notify_order(&mut self, order: OrderEvent) {
        match order.signal {
            SignalCommand::Buy(_, volume) => {
                log_event(order.timestamp, &format!("buy market {}", volume));
                self.last_buy_ts = order.timestamp;
                self.buy_orders.push(Reverse(order));
            }
            SignalCommand::Sell(order_type, volume) => {
                log_event(
                    order.timestamp,
                    &format!("sell {:?} {}", &order_type, volume),
                );
                self.sell_orders
                    .entry(order.exchange)
                    .or_insert(HashMap::new())
                    .entry(order.symbol)
                    .or_insert(vec![])
                    .push((order.timestamp, order_type, volume));
            }
            _ => {}
        }
    }

    fn update_sell(&mut self, sell_success: SellSuccess) {
        let SellSuccess(exchange, symbol, timestamp, order_type, volume) = sell_success;
        if let Some(symbol2rest) = self.sell_orders.get_mut(&exchange) {
            if let Some(sells) = symbol2rest.get_mut(&symbol) {
                sells
                    .iter_mut()
                    .filter(|(ts, ot, rest)| {
                        *ts == timestamp && *ot == order_type && *rest >= volume
                    })
                    .for_each(|(_, _, rest)| {
                        *rest -= volume;
                    });
                sells.retain(|(_, _, rest)| *rest > 0);
            }
        }
    }

    fn print_summary(&self) {
        println!("-------- strategy final summary --------");
        for (_, e2rest) in &self.sell_orders {
            for sells in e2rest.values() {
                println!("{:?}", sells);
            }
        } 
        println!("\n");
    }
}

fn log_event(ts: Timestamp, msg: &str) {
    println!("[strategy][{}]{}", ts, msg);
}

impl NaiveStrategy {
    fn buy_trigger(&mut self, market_event: &MarketEvent) -> Option<SignalEvent> {
        let current_price = get_current_price(market_event);
        let exchange = &market_event.exchange;
        let symbol = &market_event.symbol;

        if let Some(price_10min_ago) =
            self.get_past_price(market_event.timestamp, exchange, symbol, 10 * 60)
        {
            if price_increase_trigger(price_10min_ago, current_price) {
                return Some(SignalEvent::new_buy(
                    exchange.clone(),
                    symbol.clone(),
                    TEN_HAND,
                    OrderType::Market,
                    market_event.timestamp,
                ));
            }
        }
        None
    }

    fn add_price(&mut self, exchange: Exchange, symbol: String, ts: Timestamp, price: PriceType) {
        self.prices
            .entry(exchange)
            .or_insert(HashMap::new())
            .entry(symbol)
            .or_insert(VecDeque::new())
            .push_back((ts, price));
    }

    fn get_past_price(
        &mut self,
        now: Timestamp,
        exchange: &Exchange,
        symbol: &String,
        seconds: i64,
    ) -> Option<PriceType> {
        match self.prices.get_mut(exchange) {
            Some(stock_prices) => match stock_prices.get_mut(symbol) {
                Some(prices) => {
                    let mut to_return = None;
                    while let Some((ts, price)) = prices.pop_front() {
                        if (now - ts).num_seconds() < seconds {
                            // put it back
                            prices.push_front((ts, price));
                            break;
                        }
                        to_return = Some(price);
                    }
                    to_return
                }
                _ => None,
            },
            _ => None,
        }
    }
}

fn get_current_price(market_event: &MarketEvent) -> PriceType {
    match &market_event.data {
        MarketData::Tick(tick) => tick.n_price,
        MarketData::Transaction(tx) => tx.price,
    }
}

fn price_increase_trigger(prev_price: PriceType, current_price: PriceType) -> bool {
    let increase = (current_price - prev_price)/prev_price;
    if increase > 0.005 {
        true
    } else {
        // println!("no buy trigger! current price: {}, past price: {}, change {:.2}", current_price, prev_price, increase);
        false
    }
}

#[cfg(test)]
mod tests {
    use chrono::prelude::*;
    use std::collections::{HashMap, VecDeque};

    use super::NaiveStrategy;
    use toy_engine::data::Exchange;

    #[test]
    fn test_get_past() {
        let mut ns = NaiveStrategy::new();
        let mut stock2prices = HashMap::new();
        let symbol = "101".to_owned();
        stock2prices.insert(symbol.clone(), VecDeque::new());
        let past_prices = stock2prices.get_mut(&symbol).unwrap();

        let now = Utc.ymd(2022, 2, 14).and_hms(11, 11, 59);
        let (year, month, day, hour, min, sec) = (
            now.year(),
            now.month(),
            now.day(),
            now.hour(),
            now.minute(),
            now.second(),
        );
        let prices = vec![
            (
                Utc.ymd(year, month, day).and_hms(hour, min, sec - 11),
                1.0f32,
            ),
            (
                Utc.ymd(year, month, day).and_hms(hour, min, sec - 7),
                1.1f32,
            ),
            (
                Utc.ymd(year, month, day).and_hms(hour, min, sec - 1),
                1.2f32,
            ),
        ];
        for price in prices {
            past_prices.push_back(price);
        }
        let exchange = Exchange::SZSE;
        ns.prices.insert(exchange.clone(), stock2prices);

        assert_eq!(ns.get_past_price(now, &exchange, &symbol, 10), Some(1.0));
        // assert_eq!(ns.get_past_price(now, &exchange, &symbol, 5), Some(1.1));
        assert_eq!(ns.get_past_price(now, &exchange, &symbol, 2), Some(1.1));
        assert_eq!(ns.get_past_price(now, &exchange, &symbol, 2), None);
        assert_eq!(ns.get_past_price(now, &exchange, &symbol, 1), Some(1.2));
    }
}
