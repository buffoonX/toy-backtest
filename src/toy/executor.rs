use std::cmp::min;
use std::collections::HashMap;
use toy_engine::broker::error::BrokerError;
use toy_engine::broker::{
    OrderEvent, OrderGenerator, OrderType, SellSuccess, SignalCommand, SignalEvent,
};
use toy_engine::data::{BSFlag, Exchange, MarketData, MarketEvent, PriceType, Tick, Timestamp};

pub struct NaiveBuyer {
    balance: f64,
    last_tick: HashMap<Exchange, HashMap<String, Tick>>,
    last_market_price: HashMap<Exchange, HashMap<String, PriceType>>,
    sell_orders: HashMap<Exchange, HashMap<String, HashMap<OrderType, HashMap<Timestamp, u32>>>>,
}

impl NaiveBuyer {
    pub fn new(balance: f64) -> NaiveBuyer {
        NaiveBuyer {
            balance,
            last_tick: HashMap::new(),
            sell_orders: HashMap::new(),
            last_market_price: HashMap::new(),
        }
    }
}

impl NaiveBuyer {
    fn get_current_market_price(&self, exchange: &Exchange, symbol: &String) -> PriceType {
        self.last_market_price
            .get(exchange)
            .unwrap()
            .get(symbol)
            .unwrap()
            .to_owned()
    }

    fn get_ask_price(&self, exchange: &Exchange, symbol: &str, i: usize) -> PriceType {
        // 卖i价格
        if let Some(symbol2price) = self.last_tick.get(exchange) {
            if let Some(price) = symbol2price.get(symbol) {
                return price.ask_price[i as usize - 1];
            }
        }
        0 as PriceType
    }
}

fn log_event(ts: Timestamp, msg: String) {
    println!("[executor][{}]{}", ts, msg);
}

impl OrderGenerator for NaiveBuyer {
    fn generate_order(&mut self, signal: SignalEvent) -> Result<Option<OrderEvent>, BrokerError> {
        // println!("receive signal {:?}", &signal);
        let exchange = signal.exchange;
        let symbol = signal.symbol;
        match signal.command {
            SignalCommand::Buy(order_type, volume) => match order_type {
                OrderType::Market => {
                    let sub =   self.get_current_market_price(&exchange, &symbol) as f64 * volume as f64;
                    self.balance -= sub;
                    log_event(signal.timestamp, format!("successfully buy {}; - {}", volume, sub));
                    return Ok(Some(OrderEvent::new(
                        exchange,
                        symbol,
                        SignalCommand::Buy(order_type, volume),
                        signal.timestamp,
                    )));
                }
                _ => {}
            },
            SignalCommand::Sell(order_type, volume) => {
                //
                self.sell_orders
                    .entry(exchange.clone())
                    .or_insert(HashMap::new())
                    .entry(symbol.clone())
                    .or_insert(HashMap::new())
                    .entry(order_type.clone())
                    .or_insert(HashMap::new())
                    .entry(signal.timestamp)
                    .or_insert(volume);

                println!(
                    "[executor][{}]start to sell {:?} {}",
                    signal.timestamp, &order_type, volume
                );
                return Ok(Some(OrderEvent::new(
                    exchange,
                    symbol,
                    SignalCommand::Sell(order_type, volume),
                    signal.timestamp,
                )));
            }
            SignalCommand::UpdateSell(ts, from, to, volume) => {
                //
                let sell = self
                    .sell_orders
                    .get_mut(&exchange)
                    .unwrap()
                    .get_mut(&symbol)
                    .unwrap();

                if let Some(ts2volume) = sell.get_mut(&from) {
                    if let Some(v) = ts2volume.get_mut(&ts) {
                        if volume > *v {
                            println!("inconsisent: {:?}, {:?}", *v, volume);
                            panic!();
                        }
                        *v -= volume;
                        log_event(
                            signal.timestamp,
                            format!("move {:?} {} {} to {:?}; rest {}", &from, &ts, volume, &to, *v),
                        );
                        sell.entry(to)
                            .or_insert(HashMap::new())
                            .entry(ts)
                            .or_insert(volume);
                    }
                }
            }
            _ => {}
        }

        Ok(None)
    }

    fn get_closed_balance(&self) -> f64 {
        println!("--------- executor summary ---------");
        println!("unsold orders:");
        for (_, e2s) in &self.sell_orders {
            for (_, o2s) in e2s {
                for (ot, sells) in o2s {
                    println!("{:?}: {:?}", ot, sells);
                }
            }
        }
        println!("\n");
        self.balance
    }

    fn update(&mut self, market_event: &MarketEvent) -> Vec<SellSuccess> {
        let exchange = market_event.exchange.to_owned();
        let symbol = market_event.symbol.to_owned();
        let mut sells = vec![];

        // earn by sell
        match &market_event.data {
            MarketData::Transaction(tx) => {
                let mut tx_volume = tx.volume;
                if !tx.function_code {
                    match tx.bsflag {
                        BSFlag::B => {
                            let ask_price: Vec<PriceType> = (1..10)
                                .map(|i| self.get_ask_price(&exchange, &symbol, i))
                                .collect();
                            if let Some(symbol2sell) = self.sell_orders.get_mut(&exchange) {
                                if let Some(sell_order) = symbol2sell.get_mut(&symbol) {
                                    for i in 1..10 {
                                        if ask_price[i - 1] > tx.price {
                                            continue;
                                        }
                                        if let Some(ts2volume) =
                                            sell_order.get_mut(&OrderType::Limit(i))
                                        {
                                            ts2volume.retain(|ts, rest| {
                                                if *rest > 0 {
                                                    let reduce = min(*rest, tx_volume);
                                                    tx_volume -= reduce;
                                                    *rest -= reduce;
                                                    let add = 
                                                        reduce as f64 * tx.price as f64;
                                                    log_event(
                                                        market_event.timestamp,
                                                        format!(
                                                            "successfully sell limit({}) {} {}; + {}",
                                                            i, *ts, reduce, add
                                                        ),
                                                    );
                                                    self.balance += add;
                                                    sells.push(SellSuccess(
                                                        exchange.clone(),
                                                        symbol.clone(),
                                                        *ts,
                                                        OrderType::Limit(i),
                                                        reduce,
                                                    ));
                                                }
                                                *rest > 0
                                            });
                                        }
                                        if tx_volume == 0 {
                                            break;
                                        }
                                    }
                                    // check market order

                                    if tx_volume > 0 {
                                        if let Some(market_orders) = sell_order.get_mut(&OrderType::Market) {
                                            for (ts, rest) in market_orders.iter_mut() {
                                                if * rest > 0 {
                                                    let reduce = min(*rest, tx_volume);
                                                    tx_volume -= reduce;
                                                    *rest -= reduce;
                                                    let add = reduce as f64 * tx.price as f64;
                                                    log_event(
                                                        market_event.timestamp,
                                                        format!(
                                                            "successfully sell market {} {}; + {}",
                                                            *ts, reduce, add
                                                        ),
                                                    );
                                                    self.balance += add;
                                                    sells.push(SellSuccess(
                                                        exchange.clone(),
                                                        symbol.clone(),
                                                        *ts,
                                                        OrderType::Market,
                                                        reduce,
                                                    ));
                                                }
                                                if tx_volume == 0 {
                                                    break;
                                                }
                                            }
                                            market_orders.retain(|_, rest| {
                                                *rest > 0
                                            });
                                        }
                                    }
                                }
                            };
                        }
                        _ => {}
                    }
                }

                // update market price
                *self
                    .last_market_price
                    .entry(exchange.clone())
                    .or_insert(HashMap::new())
                    .entry(symbol.clone())
                    .or_insert(tx.price) = tx.price;
            }

            MarketData::Tick(tick) => {
                // update last tick
                *self
                    .last_tick
                    .entry(exchange)
                    .or_insert(HashMap::new())
                    .entry(symbol)
                    .or_insert(tick.to_owned()) = tick.to_owned();
            }
        }
        sells
    }
}
