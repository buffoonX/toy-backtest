pub mod trader;

use tokio::sync::{broadcast, oneshot};
use trader::Trader;

use crate::{
    broker::{OrderGenerator, SignalGenerator},
    data::MarketEventGenerator,
};

type TerminationMessage = String;

pub struct Engine<Data, Strategy, BrokerAgent>
where
    Data: MarketEventGenerator + Send,
    Strategy: SignalGenerator + Send,
    BrokerAgent: OrderGenerator + Send,
{
    traders: Vec<Trader<Strategy, BrokerAgent, Data>>,
    termination_rx: oneshot::Receiver<TerminationMessage>,
    traders_termination_tx: broadcast::Sender<TerminationMessage>,
}

impl<Data, Strategy, BrokerAgent> Engine<Data, Strategy, BrokerAgent>
where
    Data: MarketEventGenerator + Send + 'static,
    Strategy: SignalGenerator + Send + 'static,
    BrokerAgent: OrderGenerator + Send + 'static,
{
    pub fn new(
        traders: Vec<Trader<Strategy, BrokerAgent, Data>>,
        termination_rx: oneshot::Receiver<TerminationMessage>,
    ) -> Engine<Data, Strategy, BrokerAgent> {
        let (term_tx, _) = broadcast::channel(1);

        let traders = traders
            .into_iter()
            .map(|mut t| {
                t.add_termination_tx(term_tx.subscribe());
                t
            })
            .collect();

        Engine {
            termination_rx,
            traders_termination_tx: term_tx,
            traders,
        }
    }

    pub async fn start(self) {
        let finish =
            futures::future::join_all(self.traders.into_iter().map(|t| tokio::spawn(t.run())));

        tokio::select! {
            maybe_balances = finish => {
                maybe_balances.into_iter().enumerate().for_each(|(i, maybe_balance)| {
                    match maybe_balance {
                        Ok(v) => {println!("trader {i} balance: {}", v);},
                        Err(e) => {println!("trader {i} err: {}" , e);},
                    }
                })
            },
            v = self.termination_rx => {
                if let Err(e) = self.traders_termination_tx.send("stop".to_owned()) {
                    println!("err occurred while sending termination msg to all traders {}", e);
                } else {
                    println!("terminated: {:?}", v);
                }
            }
        };
    }
}
