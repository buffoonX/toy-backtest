use std::collections::VecDeque;

use crate::broker::{OrderEvent, OrderGenerator, SignalEvent, SignalGenerator};
use crate::data::{MarketEvent, MarketEventGenerator};
use crate::engine::TerminationMessage;

use tokio::sync::broadcast;
// use tokio::sync::broadcast::error::RecvError;

pub struct Trader<Strategy, BrokerAgent, MarketData>
where
    Strategy: SignalGenerator + Send,
    BrokerAgent: OrderGenerator + Send,
    MarketData: MarketEventGenerator + Send,
{
    termination_rx: Option<broadcast::Receiver<TerminationMessage>>,
    data: MarketData,
    broker_agent: BrokerAgent,
    strategy: Strategy,

    // 在监听到一个market事件后可能要做多个动作，这里主要负责储存并执行这些动作序列; 执行完后再监听下一个market
    event_queue: VecDeque<TraderEvent>,
}

pub enum TraderEvent {
    Market(MarketEvent),
    Signal(SignalEvent),
    Order(OrderEvent),
}

impl<Strategy, BrokerAgent, MarketData> Trader<Strategy, BrokerAgent, MarketData>
where
    Strategy: SignalGenerator + Send,
    BrokerAgent: OrderGenerator + Send,
    MarketData: MarketEventGenerator + Send,
{
    pub fn new(data: MarketData, broker_agent: BrokerAgent, strategy: Strategy) -> Self {
        Trader {
            termination_rx: None,
            data,
            broker_agent,
            strategy,
            event_queue: VecDeque::new(),
        }
    }

    pub fn add_termination_tx(&mut self, termination_rx: broadcast::Receiver<TerminationMessage>) {
        self.termination_rx = Some(termination_rx);
    }

    pub async fn run(mut self) -> f64 {
        let mut market_event_receiver = self
            .data
            .get_market_event_rx()
            .expect("market event receiver has already be taken!");
        let mut termination_rx = self.termination_rx.unwrap();

        loop {
            tokio::select! {
                e = termination_rx.recv() => {
                    println!("{:?}", e);
                    break;
                },
                event = market_event_receiver.recv() => {
                    match event {
                        Some(market_event) => {
                            // println!("receives market event: {:?}", &market_event);
                            self.event_queue.push_back(TraderEvent::Market(market_event));
                        },
                        None => {
                            println!("no more market event!");
                            break;
                        },
                    }
                },
            }

            while let Some(event) = self.event_queue.pop_back() {
                match event {
                    TraderEvent::Market(market_event) => {
                        // 可能同时触发一个买入和卖出, 因此返回一个vec
                        let signals = self.strategy.generate_signal(&market_event);
                        signals
                            .into_iter()
                            .for_each(|e| self.event_queue.push_back(TraderEvent::Signal(e)));
                        self.broker_agent
                            .update(&market_event)
                            .into_iter()
                            .for_each(|sell_success| self.strategy.update_sell(sell_success));
                    }
                    TraderEvent::Signal(signal) => {
                        match self.broker_agent.generate_order(signal) {
                            Ok(Some(order)) => {
                                // 确认买入/卖出了
                                self.event_queue.push_back(TraderEvent::Order(order));
                            }
                            _ => {}
                        }
                    }
                    TraderEvent::Order(order) => {
                        self.strategy.notify_order(order.clone());
                    }
                }
            }
        }
        self.strategy.print_summary();
        self.broker_agent.get_closed_balance()
    }
}
