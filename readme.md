# ov
- a toy backtest framework

# entities
- trader: monitor and recv market events, pass the events to strategy maker & decide whether to issue signals
  - input needed (use relation)
    - termination channel (listen from engine)
    - portfolio
    - executor(broker agent)
    - strategy maker
    - market data generator (exchange, symbol)
      - transaction or tick data
- executor: recv signals/command info from trader, contact the broker and execute the order (buy or sell)
  - process_signal: at least support buy and sell
  - also needs a portfolio to reduce/increase balance
- strategy maker: accept event info from trader and issues **zero or more signals** (typicall 0 to 2 in this case)
  - signal
    - event type
    - timestamp
    - exchange, symbol
    - signal to buy or sell
  - know whether to sell or buy by checking transaction stream
  - one market data may lead to multiple signals; e.g. one buy and one sell
